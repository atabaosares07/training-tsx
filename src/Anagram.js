import React from 'react'

class Anagram extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isAnagram : undefined,
            input1 : '',
            input2 : ''
        }
    }

    handleClick = (e) => {
        this.setState({
            isAnagram: (this.state.input1.split('').sort().join('') === this.state.input2.split('').sort().join(''))
        })
    }

    handleTextInput1Changed(e) {
        this.setState({input1: e.target.value})
    }

    handleTextInput2Changed(e) {
        this.setState({input2: e.target.value})
    }

    render() {
        return (
            <div>
                <h1>Anagram Checker</h1>
                First Text:
                <input type="text" id="input1" onChange={ (e) => this.handleTextInput1Changed(e) } />
                <br />
                Second Text:
                <input type="text" id="input2" onChange={ (e) => this.handleTextInput2Changed(e) } />
                <button onClick={this.handleClick}>Check</button>
                <br />
                <br />
                Result:
                {
                    this.state.isAnagram !== undefined ? 
                        (this.state.isAnagram ? ' Anagram' : ' Not Anagram') 
                    : ''
                }
            </div>
        )
    }
}

export default Anagram;