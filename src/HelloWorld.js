import React from 'react'

// const world = 'Philippines'
class World extends React.Component {
    render() {
        // return <div>Hello, {this.props.animal}</div>
        return (
            <div>
                Hello, {this.props.animal}
                <button>Go</button>
            </div>
        )
    }
}

export default World;