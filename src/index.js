import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';

// import World from './HelloWorld'
// import Greet from './Greet'
// import Train from './Trains'
// import Clock from './Clock'
// import RandomNumber from './RandomNumberGenerator'
// import Anagram from './Anagram'
import SchoolRegistration from './SchoolRegistration';

ReactDOM.render(
  <React.StrictMode>
    {/* <World animal="cat" /> */}
    {/* <Greet age="1" />
    <Train /> */}
    {/* <Clock /> */}
    {/* <RandomNumber stepInterval="4000" /> */}
    {/* <Anagram /> */}
    <SchoolRegistration />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
