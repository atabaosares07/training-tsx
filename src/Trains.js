import React from 'react'

class Train extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            counter: 0
        }
        console.log(this)
        debugger
    }

    componentWillMount() {
        
    }

    render() {
        return (
            <div>
                <h1>My counter is {this.state.counter}</h1>
            </div>
        )
    }

    componentDidMount() {
        console.log('Component mounted now')
        this.setState({counter: 1})
    }
}

export default Train;