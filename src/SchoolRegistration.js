import React from 'react'

class SchoolRegistration extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            schools : [],
            nameOfSchool: "",
            numberOfClasses: 0,
            numberOfStudents: 0,
            hasAircon: true
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        console.log(this.state.schools)
        if(localStorage.getItem("schools") === null) {
            this.setState({
                schools: []
            })
        }
        else {
            let schoolsFromLocalStorage = JSON.parse(localStorage.getItem("schools"))
            this.setState({schools: this.state.schools.push(schoolsFromLocalStorage)})
        }
    }

    handleSubmit(e) {
        let schoolsFromLocalStorage = localStorage.getItem("schools") === null ? [] : JSON.parse(localStorage.getItem("schools"))
        localStorage.setItem("schools", JSON.stringify(this.state));
        this.setState = ({
            nameOfSchool: "",
            numberOfClasses: 0,
            numberOfStudents: 0,
            hasAircon: true
        });
        e.preventDefault();
    }

    handleChange(e) {
        let name = e.target.name
        let value = e.target.value

        this.setState({
            [name]: e.target.type === 'radio' ? value === 'true' : value
        });
    }
    
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Name of School:
                        <input type="text" name="nameOfSchool" value={this.state.nameOfSchool} onChange={this.handleChange} />
                    </label>
                    <br />
                    <label>
                        Number of Classes:
                        <input type="text" name="numberOfClasses" value={this.state.numberOfClasses} onChange={this.handleChange} />
                    </label>
                    <label>
                        Number of Students:
                        <input type="text" name="numberOfStudents" value={this.state.numberOfStudents} onChange={this.handleChange} />
                    </label>
                    <br />
                    <label>
                        Has Aircon:
                        <input type="radio" name="hasAircon" value="true" checked={this.state.hasAircon === true} onChange={this.handleChange} /> Yes
                        <input type="radio" name="hasAircon" value="false" checked={this.state.hasAircon === false} onChange={this.handleChange} /> No
                    </label>
                    <br />
                    <button type="submit">Save</button>
                </form>
            </div>
        )
    }
}

export default SchoolRegistration;