import React from 'react'

class RandomNumber extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            number: 0,
        }
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000)
    }

    componentWillUnmount() {
        clearInterval(this.timerID)
    }

    tick() {
        this.setState({
            number: Math.floor((Math.random() * this.props.stepInterval) + 1)
        })
    }

    render() {
        return(
            <div>
                <h1>Number: {this.state.number}</h1>
            </div>
        )
    }
}

export default RandomNumber;